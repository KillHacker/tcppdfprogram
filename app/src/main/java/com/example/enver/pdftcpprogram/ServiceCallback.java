package com.example.enver.pdftcpprogram;

import java.io.File;

/**
 * Created by Enver on 30.08.2017..
 */

public interface ServiceCallback {

    void setStatus(final String status);

    void setPdf(File pdf);

    String getIpAddress();

    int getPort();
}
