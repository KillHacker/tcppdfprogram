package com.example.enver.pdftcpprogram;

import android.app.Application;

import java.io.OutputStream;

/**
 * Created by Enver on 06.08.2017..
 */

public class AppController extends Application {

    private static AppController mInstance;
    public OutputStream outputStream;

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }
}
