package com.example.enver.pdftcpprogram.Fragments;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.pdf.PdfRenderer;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.enver.pdftcpprogram.R;

import java.io.File;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by Enver on 20.07.2017..
 */

public class PdfPageFragment extends Fragment {

    Matrix matrix;
    PhotoViewAttacher photoViewAttacher;
    PdfRenderer renderer;
    File pdf;
    int pageIndex;
    private ImageView img;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.pdf_page_layout, container, false);

        Bundle args = getArguments();

        pdf = (File) args.getSerializable("pdf");
        pageIndex = args.getInt("index");

        img = (ImageView) rootView.findViewById(R.id.image);

        matrix = img.getImageMatrix();

        photoViewAttacher = new PhotoViewAttacher(img);
        photoViewAttacher.update();

        render();

        return rootView;
    }

    private void render() {
        try {
            renderer = new PdfRenderer(ParcelFileDescriptor.open(pdf, ParcelFileDescriptor.MODE_READ_ONLY));

            PdfRenderer.Page page = renderer.openPage(pageIndex);
            int width = page.getWidth();
            int height = page.getHeight();

            final Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_4444);

            Rect rect = new Rect(0, 0, width, height);

            page.render(bitmap, rect, matrix, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    img.setImageMatrix(matrix);
                    img.setImageBitmap(bitmap);

                    photoViewAttacher.update();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
