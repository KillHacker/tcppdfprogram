package com.example.enver.pdftcpprogram;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Enver on 06.08.2017..
 */

public class Utils {

    public static byte[] getBytesFromInputStream(InputStream is) throws IOException {
        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[0xFFFF];

            for (int len; (len = is.read(buffer)) != -1; )
                os.write(buffer, 0, len);

            os.flush();

            return os.toByteArray();
        }
    }

    public static String lengthenString(String in) {
        String out = in;
        for (int i = in.length(); i < 100; i++)
            out += " ";

        return out;
    }

    public static byte[] getDataFromPacket(byte[] bytes, int offset, int length) {
        byte[] result = new byte[length];
        for (int i = 0; i < length; i++) {
            result[i] = bytes[offset + i];
        }

        return result;
    }

}
