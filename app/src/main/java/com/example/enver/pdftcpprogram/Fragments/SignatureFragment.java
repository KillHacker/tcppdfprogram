package com.example.enver.pdftcpprogram.Fragments;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.enver.pdftcpprogram.Activities.MainActivity;
import com.example.enver.pdftcpprogram.AppController;
import com.example.enver.pdftcpprogram.CustomViews.DrawableView;
import com.example.enver.pdftcpprogram.R;
import com.example.enver.pdftcpprogram.Utils;

import java.io.ByteArrayOutputStream;

/**
 * Created by Enver on 05.08.2017..
 */

public class SignatureFragment extends Fragment {

    Bundle args;
    DrawableView signature;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.sign_dialog, container, false);

        args = getArguments();

        Button signButton = (Button) rootView.findViewById(R.id.signButton);
        Button clearButton = (Button) rootView.findViewById(R.id.clearButton);

        signature = (DrawableView) rootView.findViewById(R.id.signatureView);

        signButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Client().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signature.Clear();
            }
        });

        return rootView;
    }

    private class Client extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {

            try {
                Bitmap bmp = signature.canvasBitmap;
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                byte[] commandBytes = Utils.lengthenString("signature " + args.getInt("transactionId") + " " + byteArray.length).getBytes();

                final byte[] bytesToSend = new byte[byteArray.length + commandBytes.length];
                for (int i = 0; i < commandBytes.length; i++)
                    bytesToSend[i] = commandBytes[i];
                for (int i = 0; i < byteArray.length; i++)
                    bytesToSend[100 + i] = byteArray[i];

                AppController.getInstance().outputStream.write(bytesToSend);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(), "Signature sent!", Toast.LENGTH_LONG).show();
                    }
                });

                ((MainActivity) getActivity()).setStatus("Signature sent, waiting for another pdf...");
            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(), "Signature was not send!", Toast.LENGTH_LONG).show();
                    }
                });
                e.printStackTrace();
            }

            return null;
        }
    }
}
