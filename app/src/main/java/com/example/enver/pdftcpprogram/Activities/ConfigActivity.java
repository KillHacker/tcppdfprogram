package com.example.enver.pdftcpprogram.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.enver.pdftcpprogram.R;

public class ConfigActivity extends AppCompatActivity {

    String ip;
    int port;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_config);

        Button connectButton = (Button) findViewById(R.id.connectButton);
        Button configureButton = (Button) findViewById(R.id.configureButton);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        ip = sharedPreferences.getString("ip", "88.86.110.55");
        port = sharedPreferences.getInt("port", 6800);

        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(ConfigActivity.this, MainActivity.class);
                intent.putExtra("ip", ip);
                intent.putExtra("port", port);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        configureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(ConfigActivity.this);
                dialog.setContentView(R.layout.settings_dialog);

                final EditText ipView = (EditText) dialog.findViewById(R.id.ipView);
                final EditText portView = (EditText) dialog.findViewById(R.id.portView);
                Button saveButton = (Button) dialog.findViewById(R.id.saveButton);

                ipView.setText(ip);
                portView.setText(port + "");

                saveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String ipTemp = ipView.getText().toString();
                        if (ipTemp.split("\\.").length != 4) {
                            Toast.makeText(ConfigActivity.this, "Please enter a valid ip address", Toast.LENGTH_LONG).show();

                            return;
                        }

                        int portTemp;
                        try {
                            portTemp = Integer.parseInt(portView.getText().toString());
                        } catch (Exception e) {
                            Toast.makeText(ConfigActivity.this, "Please enter a valid port address", Toast.LENGTH_LONG).show();

                            return;
                        }

                        SharedPreferences.Editor sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ConfigActivity.this).edit();
                        sharedPreferences.putString("ip", ipTemp);
                        sharedPreferences.putInt("port", portTemp);
                        sharedPreferences.apply();

                        ip = ipTemp;
                        port = portTemp;

                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                System.exit(0);
            }
        }, 180000); //3 minutes
    }
}
