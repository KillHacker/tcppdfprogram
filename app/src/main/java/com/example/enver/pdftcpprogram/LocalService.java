package com.example.enver.pdftcpprogram;

import android.app.IntentService;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.enver.pdftcpprogram.Activities.MainActivity;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class LocalService extends IntentService {

    public static final int VERSION = 6;
    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();
    public int transactionId;
    Socket socket;
    OutputStream out;
    private ServiceCallback serviceCallback;

    public LocalService() {
        super("Dbg");
    }

    @Override
    public IBinder onBind(Intent intent) {
        serviceCallback = MainActivity.serviceCallback;
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (socket != null && !socket.isClosed())
            try {
                socket.close();
            } catch (Exception e) {


            }
    }

    public void Execute() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    serviceCallback.setStatus("Connecting...");

                    socket = new Socket(InetAddress.getByName(serviceCallback.getIpAddress()), serviceCallback.getPort());
                    socket.setKeepAlive(true);

                    InputStream is = socket.getInputStream();
                    InputStreamReader in = new InputStreamReader(is);

                    out = socket.getOutputStream();
                    AppController.getInstance().outputStream = out;

                    out.write(Utils.lengthenString("hello " + VERSION + " 1465499999.256475").getBytes());
                    out.write(Utils.lengthenString("sendfile").getBytes());

                    if (!socket.isClosed())
                        serviceCallback.setStatus("Connected, waiting for pdf...");

                    boolean pingingStarted = false;
                    long lastTime = 0;

                    while (!socket.isClosed()) {

                        if (pingingStarted && SystemClock.elapsedRealtime() - lastTime > 1000) {
                            Log.w("socket", "ping");
                            try {
                                out.write(Utils.lengthenString("ping 1465499999.256475").getBytes());
                            } catch (SocketException e) {
                                Log.w("socket", "error");
                            }
                            lastTime = SystemClock.elapsedRealtime();
                        } else {
                            byte[] data = Utils.getBytesFromInputStream(is);

                            String strTcp = new String(data);

                            if (!strTcp.isEmpty()) {

                                if (strTcp.startsWith("file")) {
                                    if (parseFile(strTcp, data)) {
                                        pingingStarted = true;
                                        lastTime = SystemClock.elapsedRealtime();
                                    }

                                    Log.d("pdf", "Pdf loaded");
                                }
                            }
                        }
                    }

                    serviceCallback.setStatus("Disconnected.");
                } catch (UnknownHostException unknownHostException) {
                    unknownHostException.printStackTrace();
                    serviceCallback.setStatus("Host not found, could not connect.");
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                    serviceCallback.setStatus("Could not connect.");
                }
            }
        }).start();
    }

    private boolean parseFile(String strTcp, byte[] data) {
        String tempStr = strTcp.substring(5);
        tempStr = tempStr.substring(0, tempStr.indexOf(" "));
        transactionId = Integer.parseInt(tempStr);

        tempStr = strTcp.substring(6 + (transactionId + "").length());
        tempStr = tempStr.substring(0, tempStr.indexOf(" "));
        int length = Integer.parseInt(tempStr);

        try {
            File mPdf = new File(getFilesDir() + File.separator + "data.pdf");

            if (mPdf.exists())
                mPdf.delete();

            mPdf.createNewFile();

            serviceCallback.setPdf(mPdf);

            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(mPdf));
            bos.write(Utils.getDataFromPacket(data, 100, length));
            bos.flush();
            bos.close();

            serviceCallback.setStatus(null);

            //Return true means that pinging should be started
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            serviceCallback.setStatus("Error writing pdf to internal memory.");

            try {
                if (!socket.isClosed())
                    socket.close();
            } catch (IOException i) {
                i.printStackTrace();
            }
        }

        return false;
    }

    public class LocalBinder extends Binder {
        public LocalService getService() {
            // Return this instance of LocalService so clients can call public methods
            return LocalService.this;
        }
    }
}
