package com.example.enver.pdftcpprogram.Activities;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.pdf.PdfRenderer;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.enver.pdftcpprogram.Fragments.PdfPageAdapter;
import com.example.enver.pdftcpprogram.Fragments.PdfPageFragment;
import com.example.enver.pdftcpprogram.Fragments.SignatureFragment;
import com.example.enver.pdftcpprogram.LocalService;
import com.example.enver.pdftcpprogram.R;
import com.example.enver.pdftcpprogram.ServiceCallback;

import java.io.File;

public class MainActivity extends AppCompatActivity implements ServiceCallback {

    public static ServiceCallback serviceCallback;
    File pdf;
    LocalService mService;
    boolean mBound = false;
    private String ipAddress;
    private int port;
    private ViewPager mPager;
    private PdfPageAdapter mPagerAdapter;
    private TextView pageNumber, statusText;
    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            LocalService.LocalBinder binder = (LocalService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            mService.Execute();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        serviceCallback = this;

        mPager = (ViewPager) findViewById(R.id.pager);
        pageNumber = (TextView) findViewById(R.id.pageNumber);
        statusText = (TextView) findViewById(R.id.statusText);

        Intent intent = getIntent();
        ipAddress = intent.getStringExtra("ip");
        port = intent.getIntExtra("port", 6800);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            setStatus("Permissions not granted.");

            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    146);
        } else {
            // Bind to LocalService
            Intent serviceIntent = new Intent(this, LocalService.class);
            bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    private void InvalidateViewPager() {
        try {
            mPagerAdapter = new PdfPageAdapter(getSupportFragmentManager());

            PdfRenderer pdfRenderer = new PdfRenderer(ParcelFileDescriptor.open(pdf, ParcelFileDescriptor.MODE_READ_ONLY));
            int pageCount = pdfRenderer.getPageCount();

            for (int i = 0; i < pageCount; i++) {
                Fragment fragment = new PdfPageFragment();

                Bundle args = new Bundle();
                args.putSerializable("pdf", pdf);
                args.putInt("index", i);

                fragment.setArguments(args);

                mPagerAdapter.addFragment(fragment);
            }

            Fragment fragment = new SignatureFragment();
            mPagerAdapter.addFragment(fragment);

            Bundle args = new Bundle();
            args.putInt("transactionId", mService.transactionId);
            args.putString("ip", ipAddress);
            args.putInt("port", port);
            fragment.setArguments(args);

            mPager.setAdapter(mPagerAdapter);
            //mPager.setPageTransformer(true, new ZoomOutPageTransformer());

            mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    pageNumber.setText((position + 1) + "/" + mPagerAdapter.getCount());
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });

            invalidateOptionsMenu();

            pageNumber.setText("1/" + mPagerAdapter.getCount());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 146: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // Bind to LocalService
                    Intent serviceIntent = new Intent(this, LocalService.class);
                    bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                    Toast.makeText(this, "Storage permission denied!", Toast.LENGTH_LONG).show();
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    public void setStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (status == null) {
                    statusText.setVisibility(View.GONE);
                    mPager.setVisibility(View.VISIBLE);
                    pageNumber.setVisibility(View.VISIBLE);

                    InvalidateViewPager();
                } else {
                    statusText.setVisibility(View.VISIBLE);
                    mPager.setVisibility(View.GONE);
                    pageNumber.setVisibility(View.GONE);

                    statusText.setText(status);
                }
            }
        });
    }

    @Override
    public void setPdf(File pdf) {
        this.pdf = pdf;
    }

    @Override
    public String getIpAddress() {
        return ipAddress;
    }

    @Override
    public int getPort() {
        return port;
    }

}
